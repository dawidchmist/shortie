from pickle import GET
from flask import Flask, request, jsonify
import redis

import random
import string

app = Flask(__name__)

r = redis.Redis(host='localhost', port=6379, db=0)

def get_random_string(length):
    letters = string.ascii_lowercase
    result_str = ''.join(random.choice(letters) for i in range(length))
    return result_str

@app.route("/urls", methods=['GET', 'POST'])
def url_handler():
    if request.method == 'POST':
        url = request.json.get('url')
        url_key = get_random_string(8)
        r.set(url_key, url)
        return jsonify(url_key)
    else:
        url_key = request.json.get('url_key')
        return r.get(url_key)